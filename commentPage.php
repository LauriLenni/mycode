<?php
    //Vaaditaan index.php ja connect.php, jotta valikko tulostuisi sivun alkuun sekä olisi yhteys tietokantaan.
    require_once("index.php");
    require_once("connect.php");
    
    //Vastaanotetaan muuttuja pictures.php sivulta.
    $IMGid = mysqli_real_escape_string($db, $_GET["id"]);
?>

<!DOCTYPE html>
<html>
    <body>
        <div id="imgs">
            <?php
            //Tulostetaan yksittäinen kuva näytölle.
            echo "<img src='loadImg.php?id=" . $IMGid ."'>";
            ?>
        </div>
        <div id="comments">
            <table>
                <!--Luodaan taulukko käyttäjien kommenteille -->
                <tr><td><strong>User</strong></td><td><strong>Comment</strong></td><td><strong>Date</strong></td></tr>
                <?php
                    //Ladataan kaikki komentit, joiden id on sama kuin edelliseltä sivulta vastaanotettu id.
                    $query = "SELECT * FROM comments WHERE Img_id=$IMGid";
                    $result = mysqli_query($db, $query);
                    
                    //Loopin avulla käydään haetut tiedot läpi.
                    while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){ 
                        
                        //Otetaan ylös käyttäjä, kommentti sekä aika.
                        $id = $row['ID'];
                        $user = $row['User'];
                        $comment = $row['Comment'];
                        $date = $row['Date'];
                        
                         //Tulostetaan saadut tiedot taulukkoon.
                        echo "<tr><td>" . $user . "</td><td>" . $comment . "</td><td>" . $date. "</td>";
                        if($_SESSION['username'] === $user) {
                            echo "<td><input id=" . $id . " class='delete' type='button' name='submit2' value='Delete'/></td>";
                        }
                        
                        echo "</tr>";
                    }    
                        
                ?>
            </table>    
        </div>
        <div id="commentBox">
            <!--Käyttäjä syöttää haluamansa kommentin tekstikenttään ja lähettää painamalla submit-painiketta. -->
            <!--Tiedot lähetetään formin avulla addComment.php sivulle. -->
            <form action="addComment.php" method="post">
                <input id="commentinput" type="text" name="comment" placeholder="Comment"/>
                <!--Piilossa oleva arvo on kuvan id, jotta seuraavalla sivulla tiedetään, että mitä kuvaa ollaan kommentoitu. -->
                <input type="hidden" name="imgid" value="<?php echo $IMGid?>" />
                <input type="submit" name="send" value="Submit"/>
            </form>
        </div>    
    </body>
</html>