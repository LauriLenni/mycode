<?php
    // Vaaditaan kerran index.php ja connect.php, jotta valikko tulostuisi sivun alkuun ja olisi yhteys tietokantaan.
    require_once("index.php");
    require_once("connect.php");
    
    //Tarkastetaan, että onko käyttäjä kirjautunut sisään.
    if(isset($_SESSION["username"])){
?>

<!DOCTYPE html>
<html>
    <body>
        <div>
            <?php
            //Luetaan kaikki tiedot tietokannan "pictures" listasta.
            $query = "SELECT * FROM pictures";
            $result = mysqli_query($db, $query);
            
            //Loopin avulla käydään saadut tiedot läpi sekä tulostetaan, jokainen kuva näytölle.
            while($row = mysqli_fetch_assoc($result)){ 
                //Otetaan jokaisen kuvan tunnus talteen $id muuttujaan.
                $id = $row['ID'];
                
                //Tulostetaan jokainen kuva näytölle, sekä lisätään jokaiseen kuvaan linkki toiselle sivulle.
                //Klikkaamalla kuvaa siirrytään yksittäisen kuvan näkymään, jossa käyttäjä voi lukea kommentteja sekä kommentoida.
                echo "<a href='commentPage.php?id=".$id."'><img class='images' src='loadImg.php?id=" . $id ."'></a>";
                

            }
        // Ilmoitetaan käyttäjälle, että hänen on kirjauduttava sisään seltakseen kuvia.    
        } else {
            echo "<p class='notification'>You must log in to browse images!</p>";
            
            
        }
            
        ?>
        </div>
    </body>
</html>