<?php
    require_once("index.php");
    require_once("connect.php");
    session_start();
?>

<!DOCTYPE html>

<html>
    <body>
        <div id="myProfile">
            <?php
            echo "<h3>Hello ".$_SESSION['username'] ."!</h3>";
            ?>
        </div>

        
        <div id="deleteUser">
            <h4>Delete user</h4>
            
            <!-- Kysytään käyttäjältä haluaako hän poistaa oman käyttäjätunnuksen.-->
            <!-- Lähetetään tiedot seuraavalle sivulle. -->
            <form action="deleteUserDataBase.php" method="POST">
                Are you sure? <input type="checkbox" name="confirm" value="delete">
                <input type="submit" value="Delete"/>
            </form>
            
            
        </div>    
   
    </body>
</html>