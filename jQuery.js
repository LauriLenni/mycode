$(document).ready(function(){
    
    //Kun käyttäjä klikkaa deleta-painiketta komenttia poistaessa, suoritetaan funktio.
    $(".delete").click(function(){
        var tunn = "0";
        //Määritetään muuttujan tunn arvoksi kyseisen delete-painikkeen id:n osoittama arvo.
        var tunn = $(this).attr("id");
        
        //Lähetetään remove.php sivulle äsken saadun muuttujan arvo.
        $.post("remove.php", {tunniste:tunn});
    }); 
});