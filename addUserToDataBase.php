<?php
    // Tarkastetaan, että molemmat salasanat ovat samanlaisia.
    if($_POST["pw2"] === $_POST["pw3"]) {
        
        // Asetetaan vastaanotetut tiedot muuttujiin.
        $pw2 = $_POST["pw2"];
        $username2 = $_POST["username2"];
        require_once("connect.php");
     
        // Tarkastetaan, että salasana sisältää vaaditut merkit.     
        if (!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,256}$/', $pw2)) {
            require_once("index.php");
            echo nl2br("Password is invalid! \n Password must contain following:\n 1 Uppercase \n 1 Numeric \n 8-256 Characters");
        
        } else {
            // Salataan salasana.
            $pwhash2 = sha1("$pw2". SALT);
            
            
            // Lisätään käyttäjän syöttämä arvo tietokantaan.
            $sql = mysqli_query($db, "INSERT INTO users VALUES('', '$username2', '$pwhash2')");
        
            // Lisäämisen jälkeen kutsutaan etusivua.
            require_once("login.php");
            
            echo "You have created user: $username2";    
        }
    }else {
        require_once("register.php");
        echo "Passwords must be the same!";
    }
?>