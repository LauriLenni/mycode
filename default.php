<?php
    require_once("index.php");
?>
<!DOCTYPE html>
<html>
    <body>
        <div id="intro">
            <h3>Hello world!</h3>
            <p>This website is made by Lauri Lenni-Taattola.</p>
            <p>I decided to create website like imgur or 9gag. In this website user can upload images and comment on them. User must register and log in before any further actions!</p>
        </div>
        
        <div id="xml">
            <!-- Pyydetään käyttäjältä paikka, jonka säätietoja hän haluaa tarkastella -->
            <form method="post" action="">
                <input id="city" type="text" name="city" placeholder="Add city"/>
                <input type="submit" value="Search">
            </form>
    
            <?php
            // Luodaan välimuisti
            $mem = new Memcached();
            $mem->addServer("127.0.0.1", 11211) or die("Unable to connect");
 
            
            
            // Jos käyttäjä on syöttäny tiedon niin suoritetaan seuraavat toiminnot.
            if (isset($_POST["city"])) {
                
                // Asetetaan muuttujaan käyttäjänsyöttämä paikka.
                $search = $_POST["city"];
                
                // Asetetaan osoite, josta haetaan säätiedot    
                $url = "http://weather.yahooapis.com/forecastrss?q=". $search . "&u=c";
                
                // Ladataan xml tiedot.
                $xml = simplexml_load_file($url);
                
                // Etsitään kohdat tiedostosta, joissa 'yweather'.
                $xml->registerXPathNamespace('yweather', 'http://xml.weather.yahoo.com/ns/rss/1.0');
                
                // Määritetään sijainti.
                $location = $xml->channel->xpath('yweather:location');
                
                
                // Käydään tiedot läpi
                foreach($xml->channel->item as $item){
                    $current = $item->xpath('yweather:condition');
                    $current = $current[0];
                
                    // Asetetaan arvoja muuttujiin.                
                    $date = (string)$current['date'];
                    $weather = (string)$current['text'];
                    $temp = (string)$current['temp'];
    
                    // Lisätään saadut tiedot muuttujiin tunnin ajaksi.
                    $mem->set("date", $date, 3600);
                	$mem->set("weather", $weather, 3600);
                	$mem->set("temp", $temp, 3600);
                	$mem->set("city", (string)$location[0]['city'], 3600);
                	
                }
            } 
            
            // Haetaan tietoja välimuistista ja asetetaan ne muuttujiksi.
            $result1 = $mem->get("date");
            $result2 = $mem->get("weather");
            $result3 = $mem->get("temp");
            $result4 = $mem->get("city");
    
            
            // Jos välimuistissa on tarvittavat tiedot, niin tulostetaan tiedot.
            if($result1 and $result2 and $result4 != null) {
                echo "<h4>Current Conditions</h4>";
                echo "<table><tr><td>City: " . $result4 . "</td></tr>";
                echo "<tr><td>Date: " . $result1 . "</td></tr>";
                echo "<tr><td>Weather: " . $result2 . "</td></tr>";
                echo "<tr><td>Temperature: " .  $result3 . "&deg;C</td></tr></table>";
                
            }else {
                echo "You have to select place!";
            }
            ?>
    
    
    
    
        </div>
    </body>
</html>