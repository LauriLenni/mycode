<?php
    require_once("xml.php");
    
    // Vastaanotetaan käyttäjän syöttämä paikka.
    $search = $_POST["city"];
    
    
    // Määritetään sivu, josta säätiedot haetaan.
    $url = "http://weather.yahooapis.com/forecastrss?q=". $search . "&u=c";
    
    // Ladataan tiedot xml-muodossa.
    $xml = simplexml_load_file($url);
    
    // Etsitään tiedoista kohdat 'yweather'
    $xml->registerXPathNamespace('yweather', 'http://xml.weather.yahoo.com/ns/rss/1.0');
    
    // Haettu paikka haetaan kohdasta 'yweather:location'
    $location = $xml->channel->xpath('yweather:location');
    
    // Käydään tietoja läpi
    foreach($xml->channel->item as $item){
        $current = $item->xpath('yweather:condition');
        $current = $current[0];
        
        // Asetetaan saadut tiedot muuttujiin.
        $date = (string)$current['date'];
        $weather = (string)$current['text'];
        $temp = (string)$current['temp'];
        
    
        // Asetetaan tiedot välimuistiin.
        $mem->set("date", $date);
    	$mem->set("weather", $weather);
    	$mem->set("temp", $temp);
    	$mem->set("city", (string)$location[0]['city']);
    	
    }
    

?>
