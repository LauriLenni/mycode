<?php
    // Vastaanotetaan tiedot edellieltä sivulta.
    $confirm = $_POST["confirm"];
    require_once("connect.php");
    session_start();
    $username = $_SESSION["username"];
    
    // Jos käyttäjä on rastittanut ruudun, niin tehdään seruaavat toiminnot.
    if(isset($confirm)){
    
        // Poistetaan käyttäjä tietokannasta.
        $sql = mysqli_query($db, "DELETE FROM users WHERE Username='$username'");
        
        // Tuhotaan session-muuttujat
        session_destroy();
        
        // Siirrytään kirjautumiseen
        require_once("login.php");
        
        
    }else {
        // Muuten siirrytään sivulle myprofile.php ja kerrotaan käyttäjälle, että hänen on vahvistettava toiminto.
        require_once("myprofile.php");
        echo "<p>You must confirm your action!<p>";
    }
?>