<?php
    //Yhditetään session muuttujat kyseiselle sivulle.
    session_start();
    
    //Lisätään $date-muuttujaan sen hetkinen aikaleima.
    $date = date("Y.m.d H:i:s");
    
    //Vastaanotetaan edellisellä sivulla lähetetty kommentti.
    $comment = $_POST["comment"];
    
    //Vastaanotetaan edellisellä sivulla lähetetty kuvan tunniste.
    $img_id = $_POST["imgid"];
    
    //Otetaan yhteys tietokantaan.
    require_once("connect.php");
    
    //Lisätään muuttujaan kirjautuneen käyttäjän nimi.
    $session_un = $_SESSION['username'];
    
    // Lisätään käyttäjän nimi, hänen syöttämä kommentti, päivämäärä ja kuvan tunniste tietokantaan.
    $sql = mysqli_query($db, "INSERT INTO comments VALUES('', '$session_un', '$comment', '$date', '$img_id')");

    // Lisäämisen jälkeen kutsutaan pictures.php.
    require_once("pictures.php");


?>