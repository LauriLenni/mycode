<?php
    // Jos käyttäjä on syöttänyt salasanan ja käyttäjätunnuksen niin suoritetaan seuraavat komennot.
    if(isset($_POST["username1"]) && isset($_POST["pw1"])) {
        
        // Asetetaan tietoja muuttujiin.
        $pw = $_POST["pw1"];
        $user = $_POST["username1"];
        
        // Luodaan yhteys tietokantaan.
        require_once("connect.php");
        
        // Haetaan tietokannasta käyttäjän tiedot.
        $query = "SELECT * FROM users WHERE Username= '$user'";
        $result = mysqli_query($db, $query);
        
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $dbid = $row['ID'];
        $dbun = $row['Username'];
        $dbpw = $row['Pwhash'];
        
        $verify = sha1($pw. SALT);
        
        
        // Jos salasana täsmää tietokannassa olevan kanssa.
        if ($verify === $dbpw) {
            // Lisätään session-muuttujaan käyttäjän tiedot.
            session_start();
            $_SESSION["userId"] = $dbid;
            $_SESSION["username"] = $dbun;
            require_once("index.php");

            
            
        } else {
            require_once("login.php");
            echo 'Invalid password or username.';
        }

        
    } 
    else {
        require_once("login.php");
        echo "You must enter username and password!";
    }
?>