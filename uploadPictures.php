<?php
    //Kutsutaan index.php, jotta valikko tulostuisi sivun alkuun.
    require_once("index.php");
    
    //Tarkastetaan, että käyttäjä on kirjautunut sisään.
    if(isset($_SESSION["username"])) {
?>

<!DOCTYPE html>
<html>
    <body>
        <div id="file">
            <table>
                <!--Formin avulla saadaan käyttäjän syöttämät tiedot lähetettyä seuraavalle sivulle. -->
                <!--Käyttäjä valitsee haluamansa tiedoston omalta tietokoneelta. Maksimi koko tiedostolle on 2Mb. -->
                <form action="add.php" method="post" enctype="multipart/form-data">
                    <tr><td><input type="file" name="file1"/></td></tr>
                    <tr><td><input type="text" name="description1" placeholder="Description"/></td></tr>
                    <tr><td><input type="submit" value="Upload"/></td></tr>
                </form>
            </table>
        </div>
        <?php
        //Käyttäjän on kirjauduttava sisään, jotta hän voi lisätä kuvia.
        } else {
            echo "<p class='notification'>You must log in to upload images!</p>";
        }
        
    
        ?>
    </body>
</html>
