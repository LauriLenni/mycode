<?php
    //Muodotetaan yhteys tietokantaan
    require_once("connect.php");
    
    //Otetaan vastaan edelliseltä sivulta muuttuja.
    $imgID = mysqli_real_escape_string($db, $_GET["id"]);


    //Noudetaan tietokannasta kaikki kuvat, joiden id on $imgID eli ladataan yksi kuva.
    $query = "SELECT * FROM pictures WHERE ID= $imgID";
    $result = mysqli_query($db, $query);
    
    
    //Loopin avulla käydään ladattuja tietoja läpi ja otetaan talteen kuvan data.
    while($row = mysqli_fetch_assoc($result)){ 
        $image = $row['Picture'];
    }
    header("content-type: image/jpeg");
    
    //Tulostetaan kyseinen kuva.
    echo $image;

?>