<?php
    function siteHeader(){
        
    //Määritetään SALT salasanoja varten.    
    define("SALT", "askdndfdgfdklgjdlkjfaslak");
    
    //Aloitetaan session, jotta voidaan tallentaa käyttäjän tietoja siihen.
    session_start();
?>
<!DOCTYPE html>

<html>
    <head>
        <!-- Määritetään perustiedot. Esimerkiksi linkkaukset muotoiluvälilehdelle. -->
        <title>Reaktor 2016</title>
        <meta charset="utf-8"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <link href="stylesheet_large.css" type="text/css" rel="stylesheet" media="all">
        <link href="stylesheet_small.css" type="text/css" rel="stylesheet" media="all">
        <script src="jQuery.js"></script>
    </head>
    <body>
        <?php
        }
        function siteNavigation() {
        ?>
        <h1>Reaktor<span id="Header1"> - Summer Jobs 2016</span></h1>
        
        <!-- Luodaan valikko, jonka avulla voidaan siirtyä eri sivujen välillä. -->
        <!-- Valikko on hieman erilainen riippuen käyttäjän statuksesta eli onko hän kirjautunut sisään vai ei.-->
        <div class="menu">
            <ul>
                <li>
                    <a href="default.php">Home</a>
                </li>
                <li>
                    <a href="pictures.php">Browse</a>
                </li>
                <li>
                    <a href="uploadPictures.php">Upload</a>
                </li>
                <?php
                // Mikäli käyttäjä on kirjautunut sisään, valikossa on vaihtoehto kirjautua ulos.
                if(isset($_SESSION["username"])){
                ?>    
                    <li>
                        <a href='myprofile.php'>My profile</a>
                    </li>
                    <li>
                        <a href='logout.php'>Log out</a>
                    </li>
                <?php
                // Jos käyttäjä ei ole kirjautunut sisään, valikossa on vaihtoehdot sisäänkirjautumiselle ja rekisteröitymiselle.
                } else {
                ?>
                <li>
                    <a href="login.php">Log In</a>
                </li>
                <li>
                    <a href="register.php">Register</a>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    
        <?php


        }
 
    ?>
    </body>
    
</html>
